#importing libraries used in the code
import pandas as pd
from env import filename
import multiprocessing as mp


#variables used in code file
color_start = "\033[1;37m"
color_end = "\033[0m"


print(color_start,'######## EDA ########',color_end)
#EDA process
data = pd.read_csv(filename,names=['passenger_id', 'flight_id', 'from_airport_IATA/FAA_code', 'destination_airport_IATA/FAA_code', 'departure_time_GMT', 'total_flight_time_min'])

print('Top 5 rows of Input File :\n', data.head(),'\n')
print('Type of Columns of Input File :\n',data.dtypes,'\n')
print('Description of Input File :')
data.info()
print('\n Number of rows, columns in Input File',data.shape)
#checking and removivng the duplicate rows in the dataset
print('Removing the duplicate rows present in the dataset')
data = data.drop_duplicates()
print('\n New number of rows, columns in Input File',data.shape,'\n')

#map/reduce functions
def mapper(val):
    try:
        #checking if the value contains the correct passenger_id format
        if type(val) == str and val.isalnum() :
            #returning the key-value pair as (passenger_id,count)
            return (val, 1)
    except Exception as e:
        #using exceptions for debugging
        raise Exception("Something went wrong in mapper function")


def shuffle(mapper_out):
    try:

        data = {}
        mapper_out = list(filter(None, mapper_out))
        for item, val in mapper_out:
            if item not in data:
                data[item] = [val]
            else:
                data[item].append(val)
        #group together all values with the same key
        return data
    except Exception as e:
        #using exceptions for debugging
        raise Exception("Something went wrong in shuffle function")

def reducer(items):
    try:
        item, count = items
        #counting the number of key occurences
        return (item, sum(count))
    except Exception as e:
        #using exceptions for debugging
        raise Exception("Something went wrong in reducer function")

def output_data(items):
    try:
        max_flights = items[0][1]

        print(color_start,'The Passenger(s) with maximum number of flights : \n', color_end)
        print('Passenger ID      ', 'Number of Flights')
        for item, val in items:
            if(val == max_flights):
                print(item,'       ', val)

        print(color_start,'\n\nNumber of flights taken per Passenger: \n', color_end)
        print('Passenger ID      ', 'Number of Flights')

        for item, val in items:
            print(item,'       ', val)
    except Exception as e:
        raise Exception("Something went wrong outputing data function")


if __name__ == '__main__':

    map_in = data['passenger_id']

    print(color_start,'######## Map/Reduce ########',color_end)
    #creating multithreading
    with mp.Pool(processes=mp.cpu_count()) as pool:

        print('Using Chunk size for Multi Threaded   :', int(len(map_in)/mp.cpu_count()))
        map_out = pool.map(mapper, map_in, chunksize=int(len(map_in)/mp.cpu_count()))
        #print(map_out)
        reduce_in = shuffle(map_out)
        #print(reduce_in)
        reduce_out = pool.map(reducer, reduce_in.items(), chunksize=int(len(reduce_in.keys())/mp.cpu_count()))
        #print(reduce_out)
        reduce_out.sort(key=lambda a: a[1], reverse=True)
        output_data(reduce_out)
