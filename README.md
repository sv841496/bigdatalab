# CSMBD21 - Cloud Computing Coursework

This coursework focuses on implementing a MapReduce executable prototype using python such that it emulates the functionality of a MapReduce/Hadoop framework
- To determine Passenger(s) with Highest Number of Flights

## Solution

The solution code file is located on the root directory named as **_main.py_**
The input csv used for this task is also present on the root directory as **_AComp_Passenger_data_no_error.csv_**
The configuration file containing the input filename is *_env.py_*

## Executing
Before executing, pass the input filename in *env.py* file as value.
such as:
*filename = 'AComp_Passenger_data_no_error.csv'*

Then to execute the code file:
*python main.py*
